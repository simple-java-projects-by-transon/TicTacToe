import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class GameSelect {

    public String logged_in_user = "";


    public void get_user(String username){
        logged_in_user = username;
        System.out.println(logged_in_user);

    }

    @FXML
    private Button join_game;

    @FXML
    private Button create_game;



    @FXML
    protected void login(ActionEvent event) throws IOException {
        Parent login_parent = FXMLLoader.load(getClass().getResource("login.fxml"));
        Scene login_scene = new Scene(login_parent);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(login_scene);
        window.setTitle("Tic Tac Toe Login");
        window.show();
    }


    @FXML
    protected void join_game(ActionEvent event) throws Exception {


    }

    @FXML
    protected void create_game(ActionEvent event) throws Exception {
        create_game_sql dao = new create_game_sql();
        dao.readDataBase(logged_in_user);
        System.out.println("Created game");

        get_gid a = new get_gid();
        String game_id = a.readDataBase(logged_in_user);
        System.out.println(game_id);

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("game.fxml"));

        Parent root = fxmlLoader.load();
        Game controller = fxmlLoader.getController();
        controller.get_player1(logged_in_user);

        Scene register_scene = new Scene(root);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(register_scene);
        window.setTitle("Tic Tac Toe");

        window.show();


    }

}