import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.stage.Stage;

import java.io.IOException;

public class Game {


    public String player1 = "";


    public void get_player1(String player) {
        player1 = player;
        System.out.println(player1);

    }


        @FXML
    private Button top_left_button;
    @FXML
    private Button top_middle_button;
    @FXML
    private Button top_right_button;
    @FXML
    private Button middle_left_button;
    @FXML
    private Button middle_button;
    @FXML
    private Button middle_right_button;
    @FXML
    private Button bottom_left_button;
    @FXML
    private Button bottom_middle_button;
    @FXML
    private Button bottom_right_button;

    public String logged_in_user = "";


    public void get_user(String username){
        logged_in_user = username;
        System.out.println(logged_in_user);

    }



    @FXML
    protected void top_left_button_action() {
        top_left_button.setText("X");
    }

    @FXML
    protected void top_middle_button_action(){
        top_middle_button.setText("X");
    }

    @FXML
    protected void top_right_button_action(){
        top_right_button.setText("X");
    }

    @FXML
    protected void middle_left_button_action(){
        middle_left_button.setText("X");
    }

    @FXML
    protected void middle_button_action(){
        middle_button.setText("X");
    }

    @FXML
    protected void middle_right_button_action(){
        middle_right_button.setText("X");
    }

    @FXML
    protected void bottom_left_button_action(){
        bottom_left_button.setText("X");
    }

    @FXML
    protected void bottom_middle_button_action(){
        bottom_middle_button.setText("X");
    }

    @FXML
    protected void bottom_right_button_action(){
        bottom_right_button.setText("X");
    }

}