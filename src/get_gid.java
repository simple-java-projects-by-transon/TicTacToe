import java.sql.*;


public class get_gid {
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private ResultSet resultSet2 = null;

    public String readDataBase(String username) throws Exception {
        String gid = "";

        try {




            Class.forName("org.mariadb.jdbc.Driver");

            connect = DriverManager
                    .getConnection("jdbc:mariadb://localhost/feedback?"
                            + "user=sqluser&password=sqluserpw");

            // Statements allow to issue SQL queries to the database
            statement = connect.createStatement();
            // Result set get the result of the SQL query
            resultSet = statement
                    .executeQuery("USE feedback");

            preparedStatement = connect
                    .prepareStatement("select * from games WHERE player1 = ? ORDER BY id DESC LIMIT 1");

            preparedStatement.setString(1, username);

            resultSet = preparedStatement.executeQuery();

            gid = view_games(resultSet, username);

        } catch (Exception e) {
            throw e;
        } finally {
            close();
        }
        return gid;
    }



    private String view_games(ResultSet resultSet, String username) throws SQLException {
        // ResultSet is initially before the first data set
        String gid = "";
        while (resultSet.next()) {

            gid = resultSet.getString("id");

        }
        return gid;

    }

    // You need to close the resultSet
    private void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }






}