import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class Register {

    @FXML
    private TextField username_field;

    @FXML
    private PasswordField password_field;


    @FXML
    private PasswordField password_confirm_field;

    @FXML
    private Button login_button;

    @FXML
    private Button register_button;



    @FXML
    protected void login(ActionEvent event) throws IOException {
        Parent login_parent = FXMLLoader.load(getClass().getResource("login.fxml"));
        Scene login_scene = new Scene(login_parent);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(login_scene);
        window.setTitle("Tic Tac Toe Login");
        window.show();
    }


    @FXML
    protected void handleSubmitButtonAction(ActionEvent event) throws Exception {


        String username = username_field.getText();
        String password = password_field.getText();
        String password_confirmation = password_confirm_field.getText();


        if(username.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Gotta have a name");

            alert.showAndWait();
        }


        else if(password.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Gotta enter a password");

            alert.showAndWait();
        }

        else if(password_confirmation.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Gotta enter a password confirmation");

            alert.showAndWait();
        }

        else {
            System.out.println("All data entered");

            if (!password.equals(password_confirmation)){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setContentText("Passwords don't match");
                alert.showAndWait();
            }

            else{
                System.out.println("Passwords match");

                register_mysql dao = new register_mysql();
                if (dao.readDataBase(username, password) == true){
                    System.out.println("Registration Successful");
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("game_select.fxml"));

                    Parent root = fxmlLoader.load();
                    GameSelect controller = fxmlLoader.getController();
                    controller.get_user(username);

                    Scene register_scene = new Scene(root);
                    Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
                    window.setScene(register_scene);
                    window.setTitle("Tic Tac Toe Game Select");

                    window.show();
                };
            }
        }



    }



}