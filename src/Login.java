import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class Login {

    @FXML
    private TextField username_field;

    @FXML
    private PasswordField password_field;



    @FXML
    private Button login_button;

    @FXML
    private Button register_button;

    public Login() {
    }




    @FXML
    protected void register(ActionEvent event) throws IOException {
        System.out.println("Clicked register button");

        Parent register_parent = FXMLLoader.load(getClass().getResource("register.fxml"));
        Scene register_scene = new Scene(register_parent);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(register_scene);
        window.setTitle("Tic Tac Toe Registration");

        window.show();
    }


    @FXML
    protected void login(ActionEvent event) throws Exception {
        String username = username_field.getText();
        String password = password_field.getText();


        if(username.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Gotta have a name");

            alert.showAndWait();
        }


        else if(password.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Gotta enter a password");

            alert.showAndWait();
        }


        else {
            System.out.println("All data entered");

            login_mysql dao = new login_mysql();
            if (dao.readDataBase(username, password) == true) {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("game_select.fxml"));

                Parent root = fxmlLoader.load();
                GameSelect controller = fxmlLoader.getController();
                controller.get_user(username);

                Scene register_scene = new Scene(root);
                Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
                window.setScene(register_scene);
                window.setTitle("Tic Tac Toe Game Select");

                window.show();
            }




        }



    }
}